![](https://github.com/gluester/SunskeECO/blob/master/SunskeEcoLogo.png)

# SunskeECO
**W.I.P**
Meet SunskeECO- an Essentials replacement!

# Requires...
* Skript
* Potentially skQuery
* Vault
* (for OTSDE edition) a Skript compatible economy system

# How to download
* Download the version you want
* In your servers' file manager, go to Plugins > Skript > Scripts > New > 'SunskeECO.sk' (Or *upload* the Skript.)
* In game, use the command /sk reload SunskeECO

# Thanks to..
The Minehut Community, *http://minehut.com* (Their Discord + Forums) and skUnity community *http://skunity.com* (Their Discord + Forums) and skUnitys' Parser. *https://parser.skunity.com*
